export interface TraitDie {
  sides: number;
  modifier: number;
}

export interface WildDie {
  sides: number;
}
