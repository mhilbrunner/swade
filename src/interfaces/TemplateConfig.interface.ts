export interface TemplateConfig {
  button: SceneControlTool;
  data: foundry.documents.BaseMeasuredTemplate.ConstructorData;
}
